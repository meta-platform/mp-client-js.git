# mp-client-js

#### 介绍
客户端JSSDK

#### 安装教程

```
npm i mp-client-js
```

或直接引用服务器上的UMD版本：
```
<script src="/client.umd.js"></script>
```

#### 使用说明

在node中：
```
import client from 'mp-client-js';

...

const data = await client.job(name, param);
```

浏览器中引入UMD版本后，使用全局client变量：
```
const k = client.get("key")
```

支持的api:

* has(key) :boolean 是否存在缓存项

* get(key) :object 获取缓存项

* set(key, value, do_remove) :void 设置缓存项

* fetch(url, init, is_json) :promise 封装后的fetch请求方法

* init_password(username, password) :promise 初始化密码

* change_password(oldPassword, newPassword) :promise 修改密码

* login(username, password) :promise 登录

* logout() :void 注销登录

* job(name, param, json_parser) :promise 调用后台job

* doc.href(file) :string 获取当前会话中的文件访问地址

* doc.download(file, rename) :void 下载文件

* doc.upload_files(dir, attr, validator) :promise 上传文件, 支持批量上传多个文件

* doc.upload_base64(data, ext, dir) :promise 上传base64数据

* doc.upload_blocks(dir, attr, validator) :promise 分块上传大文件

* doc.copy(file, dir) :promise 复制文件

* doc.delete(file) :promise 删除文件

* import(name, param, validator) :promise 导入数据

* export.xls(name, param, template) :void 导出Excel格式

* export.pdf(name, param, template) :void 导出PDF格式

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

