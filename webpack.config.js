const path = require('path');

module.exports = {
  entry: {
    index: './index.js'
  },
  output: {
    filename: 'client.umd.js',
    path: path.resolve(__dirname, 'dist'),
    library: 'client',
    libraryTarget: 'umd',
    libraryExport: 'default',
  },
  mode: 'production'
}